<?php


class Training_Animal_Adminhtml_AnimalController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_redirect('*/*/list');

    }

    public function listAction()
    {
        $this->_getSession()->setFormData(array());

        $this->_title($this->__('catalog'))
            ->_title($this->__('Animal'));

        $this->loadLayout();

        $this->_setActiveMenu('catalog/training');
        $this->_addBreadcrumb($this->__('Catalog'), $this->__('Catalog'));
        $this->_addBreadcrumb($this->__('Animal'), $this->__('Animal'));

        $this->renderLayout();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/sessios')
            ->isAllowed('catalog/training');
    }

    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $model = Mage::getModel('training/animal');

        Mage::register('current_animal', $model);

        $id = $this->getRequest()->getParam('id');

        try{
            if($id){
                if(!$model->load('id')->getId()){
                    Mage::throwException($this->__('No record with Id "%s" found', $id));
                }
            }

            if($model->getId()){
                $pageTitle = $this->__('Edit %s [%s]', $model->getName(), $model->getType());
            } else {
                $pageTitle = $this->__('New Animal');
            }

            $this->_title($this->__('catalog'))
                 ->_title($this->__('Animal'))
                 ->_title($pageTitle);
            $this->loadLayout();

            $this->_setActiveMenu('catalog/training');
            $this->_addBreadcrumb($this->__('Catalog'), $this->__('Catalog'));
            $this->_addBreadcrumb($this->__('Animal'), $this->__('Animal'));
            $this->_addBreadcrumb($pageTitle, $pageTitle);

            $this->renderLayout();
        }
        catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/list');
        }

    }

    public function saveAction()
    {
        if($data = $this->getRequest()->getPost()){
            $this->_getSession()->setFoormData($data);

            $model = Mage::getModel('training/animal');

            $id = $this->getRequest()->getParam('id');

            try{
                if($id) {
                    $model->load('id');
                }
                $model->addData($data);
                $model->save();

                $this->_getSession()->addSuccess($this->__('Animal was succsessfuly saved'));
                $this->_getSession()->setFormData('false');

                if($this->getRequest()->getParam('back')){
                    $params = array('id' =>$model->getId());
                    $this->_redirect('*/*/edit', $params);
                } else {
                    $this->_redirect('*/*/list');
                }
            } catch (Exception $e){
                $this->_getSession()->addError($e->getMessage());
                if($model && $model->getId()) {
                    $this->_redirect('*/*/edit', array($model->getId()));
                } else {
                    $this->_redirect('*/*/new');
                }
            }
            return;
        }
        $this->_getSession()->addError($this->__('No data found to save'));
        $this->_redirect('*/*');

    }

    public function deleteAction()
    {
        $model = Mage::getModel('training/animal');
        $id = $this->getRequest()->getParam('id');

        try{

            if($id){
                if(! $model->load('id')->getId()){
                    Mage::throwException($this->__('No record with id %s found', $id));
                }

                $name = $model->getName();
                $model->delete();
                $this->_getSession()->addSuccess($this->__('%s with Id %d was successfuky consumed', $id, $name));
                $this->_redirect('*/*');
            }
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*');
        }
    }

}