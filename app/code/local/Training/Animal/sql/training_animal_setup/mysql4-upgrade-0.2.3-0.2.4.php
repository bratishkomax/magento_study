<?php


$installer = Mage::getResourceModel('catalog/setup','default_setup');

$installer->startSetup();

$installer->updateAttribute(
    'catalog_product',
    'attribute_installed',
    'backend_model',
    'eav/entity_attribute_backend_array'
);

$installer->endSetup();
