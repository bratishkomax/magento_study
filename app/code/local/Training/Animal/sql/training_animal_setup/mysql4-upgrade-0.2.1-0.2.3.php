<?php


$installer = Mage::getResourceModel('catalog/setup','default_setup');

$installer->startSetup();

$data = array(
    'label' => 'Script Generated MS Option',
    'type' => 'varchar',
    'input' => 'multiselect',
    'required' => 0,
    'user_defied' => 1,
    'group' => 'Price',
    'option' => array(
        'order' => array('one' => 1, 'two' => 2, 'three' => 5),
        'value' => array(
            'one' => array(0 => 'Some label one', 2 => 'Alt One'),
            'two' => array(0 => 'Some label two', 2 => 'Alt Two'),
            'three' => array(0 => 'Some label three', 2 => 'Alt Three'),
        ),
    )
);

$installer->addAttribute('catalog_product', 'attribute_installed', $data);

$installer->endSetup();
