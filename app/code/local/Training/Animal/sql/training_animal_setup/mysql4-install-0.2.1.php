<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 27.09.17
 * Time: 13:10
 */

$installer = $this;

$installer->startSetup();

$installer-> run("
    DROP TABLE IF EXISTS {$installer->getTable('training/animal')};
    CREATE TABLE {$installer->getTable('training/animal')} (
    entity_id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL DEFAULT '',
    type VARCHAR(255) NOT NULL DEFAULT '',
    edible TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
    comment TEXT NULL,
    updated_at DATETIME,
    created_at DATETIME
    ) Engine=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();

/*
 *
    DROP TABLE IF EXISTS {$installer->getTable('training/animal')};
    CREATE TABLE {$installer->getTable('training/animal')} (
    entity_id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL DEFAULT '',
    type VARCHAR(255) NOT NULL DEFAULT '',
    edible TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
    comment TEXT NULL,
    updated_at DATETIME,
    created_at DATETIME
    ) Engine=InnoDB DEFAULT CHARSET=utf8;
 *
 * */