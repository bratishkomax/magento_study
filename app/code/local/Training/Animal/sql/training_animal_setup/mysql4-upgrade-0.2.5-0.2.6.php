<?php


$installer = Mage::getResourceModel('catalog/setup','default_setup');

$installer->startSetup();

$installer->updateAttribute(
    'catalog_product',
    'attribute_installed',
    'frontend_model',
    'training/entity_attribute_frontend_list'
);

$installer->updateAttribute(
    'catalog_product',
    'attribute_installed',
    'is_html_allowed_on_front',
    1
);

$installer->endSetup();
