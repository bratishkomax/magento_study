<?php


$installer = Mage::getResourceModel('catalog/setup','default_setup');

$installer->startSetup();

$installer->updateAttribute(
    'catalog_product',
    'attribute_installed',
    'is_visible_on_front',
    1
);

$installer->endSetup();
