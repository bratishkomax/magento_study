<?php

class Training_Animal_Block_Adminhtml_Animal_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('animal_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Item Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => $this->__('Item Information'),
            'title' => $this->__('Item Information'),
            'content' => $this->getLayout()->createBlock('animal/adminhtml_animal_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}
