<?php

class Training_Animal_Block_Adminhtml_Animal_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'animal';
        $this->_controller = 'adminhtml_animal';
        $this->_mode = 'edit';
        $this->_updateButton('save', 'label', $this->__('Save Item'));
        $this->_updateButton('delete', 'label', $this->__('Delete Item'));
   }

    public function getHeaderText()
    {
        if(Mage::registry('animal_data') && Mage::registry('animal_data')->getId()) {
        return $this->__("Edit Item '%s'", $this->escapeHtml(Mage::registry('animal_data')->getTitle()));
      }

      return $this->__('Add Item'); }
}
