<?php

class Training_Animal_Block_Adminhtml_Animal_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getTestsData()) {
            $data = Mage::getSingleton('adminhtml/session')->getTestsData();
         Mage::getSingleton('adminhtml/session')->setTestsData(null);
      } elseif (Mage::registry('animal_data')) {
            $data = Mage::registry('animal_data')->getData();
        }

        $fieldset = $form->addFieldset('animal_form', array('legend'=>$this->__('Item information')));

        $fieldset->addField('title', 'text', array(
            'label' => $this->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));
        $form->setValues($data);

        return parent::_prepareForm();
    }
}