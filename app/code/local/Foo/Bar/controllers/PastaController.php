<?php

class Foo_Bar_PastaController extends Mage_Core_Controller_Front_Action
{
    public function sleepAction()
    {
        echo " <h1> I\'m so tired after eating yumy pasta </h1>";
    }

    public function layoutAction()
    {
        $xml = $this->loadLayout()->getLayout()->getUpdate()->asString();

       // $this->getResponse()->setHeader('Content-Type', 'text/plane')->setBody($xml);

        //Mage::Log($xml,Zend_Log::NOTICE, 'layout_log.xml', true);
        $model = Mage::getModel('foo_bar/observer');
        Mage::Log($model,Zend_Log::NOTICE, 'model.log', true);
    }

    public function defaultAction()
    {
        $this->loadLayout()->renderLayout();


    }
}