<?php
/**
 *   local/Company/Module/controllers/ModelsController.php
 */

class Company_Module_ModelsController extends Mage_Core_Controller_Front_Action
{
    public function storesAction ()
    {

        $stores = Mage::getResourceModel('core/store_collection');

        echo '<h2 style="color: red">' . get_class($stores) . '</h2>';

        foreach ($stores as $store)
        {
            $category = Mage::getModel('catalog/category')->load($store->getRootCategoryId());


            echo '<h3>' . $category->getName() . '</h3>';

            /*echo '<h2 style="color: green">' . get_class($store) . ' ' . $store->getRootCategoryId() . '</h2>';

            echo '<h2>'. $store->getName(). ' ' . $store->getCode() .'</h2>';*/
        }

    }

    public function categoriesAction()
    {
        $categories = Mage::getResourceModel('catalog/category_collection')
                        ->addFieldToFilter('level', 2)
                        ->addAttributeToSelect('name');

        foreach ($categories as $category)
        {
            echo '<h2>' . $category->getId() . ' ' . $category->getName() . '</h2>';

            $children = $category->getChildren();

            $childrensIds = explode('.', $children);

            foreach ($childrensIds as $child)
            {
                $child = Mage::getModel('catalog/category')->load($child);
                Zend_Debug::dump($child->debug());
            }
        }
    }
}