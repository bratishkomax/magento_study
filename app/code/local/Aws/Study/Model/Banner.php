<?php

class Aws_Study_Model_Banner extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('aws_study/banner');
    }

    public function getClass()
    {
        return __CLASS__;
    }
}