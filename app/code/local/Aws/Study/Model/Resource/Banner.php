<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 28.09.17
 * Time: 14:14
 */

class Aws_Study_Model_Resource_Banner extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('aws_study/banner','aws_study_id');
    }
}