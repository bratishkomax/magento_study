<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 28.09.17
 * Time: 14:40
 */

class Aws_Study_Model_Resource_Banner_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
       $this->_init('aws_study/banner');
    }
}