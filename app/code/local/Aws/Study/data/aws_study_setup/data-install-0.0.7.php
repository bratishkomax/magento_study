<?php
/**
 * data-install-0.0.7
 * User: max
 * Date: 28.09.17
 * Time: 18:43
 */

$installer = $this;

Mage::getModel('core/url_rewrite')
->setStoreId(1)
->setIdPath('1234567')
->setRequestPath('banners.html')
->setTargetPath('aws/study/index')
->setIsSystem(0)
->save();