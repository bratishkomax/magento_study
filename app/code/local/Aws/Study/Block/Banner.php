<?php

class Aws_Study_Block_Banner extends Mage_Core_Block_Template
{
    public function getBlockClass()
    {
        return __CLASS__;
    }

    public function getModelClass()
    {
        return get_class(Mage::getModel('aws_study/banner'));
    }

    public function getHelperClass()
    {
        return get_class(Mage::helper('aws_study'));
    }
}