<?php

class Aws_Study_Block_Adminhtml_Banner extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function _construct()
    {
        $this->_controller =
        $this->_blockGroup = 'banners';
        parent::_construct();
    }
}
