<?php
/**
 * aws_study_setup/mysql4-install-0.0.7*/


$installer = $this;

$installer->startSetup();

$installer->run("
        DROP TABLE IF EXISTS {$installer->getTable('aws_study/banner')};
        CREATE TABLE {$installer->getTable('aws_study/banner')} (
        aws_study_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL DEFAULT '',
        description TEXT NOT NULL DEFAULT '',
        status SMALLINT NOT NULL DEFAULT 0,
        created_time DATETIME NULL,
        updated_time DATETIME NULL,
        PRIMARY KEY (aws_study_id)
        )ENGINE=Innodb DEFAULT CHARSET=utf8;

");

$installer->endSetup();

/*
 *
 */